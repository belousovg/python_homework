"""
labGameCalcHHMM - Создать мини-игру сложения двух значений времени в формате HH:MM.
Режим игры:
 - ответ в минутах;
 - ответ в формате HH:MM (часы:минуты);
Уровень сложности:
 - детский:    ответ число от 0 до 60 (это 1 * 60);
 - легкий:    ответ число от 0 до 180 (это 3 * 60);
 - средний:    ответ число от 0 до 480 (это 8 * 60);
 - сложный:    ответ число от 0 до 1440 (это 24 * 60).
В начале игры выбрать: уровень сложности, количество вопросов (например, 10).
Данные генерируются случайным образом в формате HH:MM (часы:минуты), но минуты должны быть кратны 5.
Ответом будет строка в формате “:” или целое число в зависимости от режима игры.
Пример:
    1:50 + 2:15 = ? HH:MM (Ответ: 4:05)
    1:50 + 2:15 = ? MM (Ответ: 245 (т.е это 110 + 135))
В конце игры показать статистику игры, например, количество правильных и неправильных ответов, общее время и др.
"""

import random
from datetime import datetime


def difficulty(option):
    time_1, time_2 = 0, 0
    match option:
        case "детский":
            time_1, time_2 = random.randint(0, 12)*5, random.randint(0, 12)*5
        case "легкий":
            time_1, time_2 = random.randint(0, 36)*5, random.randint(0, 36)*5
        case "средний":
            time_1, time_2 = random.randint(0, 96)*5, random.randint(0, 96)*5
        case "сложный":
            time_1, time_2 = random.randint(0, 288)*5, random.randint(0, 288)*5
        case _:
            raise Exception('выберите правильную сложность')

    return time_1, time_2


def calc_hhmm(dfclt='легкий', q_number=5, mod='minutes'):
    start_time = datetime.now()
    stats = {'right': 0, 'wrong': q_number,
             'time': start_time, 'correct_questions': [], 'wrong_questions': []}

    for i in range(q_number):
        time_1, time_2 = difficulty(dfclt)
        h1, m1 = divmod(time_1, 60)
        h2, m2 = divmod(time_2, 60)

        ans1 = time_1 + time_2
        ans_h, ans_m = divmod(ans1, 60)
        ans2 = f'{ans_h}:{ans_m}'

        print(f'{h1}:{m1} + {h2}:{m2} = ?')
        player_ans = input('Введите ответ: ')

        match player_ans, mod:
            case str(ans1), 'minutes':
                stats['right'] += 1
                stats['wrong'] -= 1
                stats['correct_questions'].append(i)
            case _, 'minutes':
                stats['wrong_questions'].append(i)
            case ans2, 'hh:mm':
                stats['right'] += 1
                stats['wrong'] -= 1
                stats['correct_questions'].append(i)
            case _, 'hh:mm':
                stats['wrong_questions'].append(i)

    end_time = datetime.now()
    stats['time'] = str(end_time - stats['time'])

    print(stats)


dfclt = input('Выберите уровень сложности: ')
q_number = input('Выберите кол-во вопросов: ')

calc_hhmm(dfclt, int(q_number))
