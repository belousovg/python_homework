"""
labGameConvertByte - Создать мини-игру конвертор двоичного числа в десятичное число или шестнадцатеричное число.
Режим игры:
 - ответ десятичным числом;
 - ответ шестнадцатеричным числом.
Уровень сложности:
 - детский:    числа от 0000 0000 до 0000 0111;
 - легкий:    числа от 0000 0000 до 0000 1111;
 - средний:    числа от 0000 0000 до 0011 1111;
 - сложный:    числа от 0000 0000 до 1111 1111. 
В начале игры выбрать: уровень сложности, количество вопросов (например, 10).
Числа генерируются случайным образом.
Пример:
    0000 1111 = ? dec (Ответ: 15)
    0000 1111 = ? hex (Ответ: 0F)
В конце игры показать статистику игры, например, количество правильных и неправильных ответов, общее время и др., а также предложить повторить игру.

"""

import random
from datetime import datetime


def difficulty(option):
    number = 0
    match option:
        case "детский":
            number = random.randint(0, 8)
        case "легкий":
            number = random.randint(0, 16)
        case "средний":
            number = random.randint(0, 64)
        case "сложный":
            number = random.randint(0, 256)
        case _:
            raise Exception('выберите правильную сложность')

    return number


def convert_byte(dfclt='легкий', q_number=5, mod='dec'):
    start_time = datetime.now()
    stats = {'right': 0, 'wrong': q_number,
             'time': start_time, 'correct_questions': [], 'wrong_questions': []}

    for i in range(q_number):
        number = difficulty(dfclt)

        ans1 = f'{number}'
        ans2 = f'{number:x}'

        print(f'{number:b} = ? {mod}')
        player_ans = input('Введите ответ: ')

        match player_ans, mod:
            case str(ans1), 'dec':
                stats['right'] += 1
                stats['wrong'] -= 1
                stats['correct_questions'].append(i)
            case _, 'dec':
                stats['wrong_questions'].append(i)
            case str(ans2), 'hex':
                stats['right'] += 1
                stats['wrong'] -= 1
                stats['correct_questions'].append(i)
            case _, 'hex':
                stats['wrong_questions'].append(i)

    end_time = datetime.now()
    stats['time'] = str(end_time - stats['time'])

    print(stats)


dfclt = input('Выберите уровень сложности: ')
q_number = input('Выберите кол-во вопросов: ')

convert_byte(dfclt, int(q_number), mod='hex')
