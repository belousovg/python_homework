import random


def frame():
    map = [['* ' * 7],
           ['*', 0, 0, 0, 0, 0, '*'],
           ['*', 0, 0, 0, 0, 0, '*'],
           ['*', 0, 0, 0, 0, 0, '*'],
           ['*', 0, 0, 0, 0, 0, '*'],
           ['* ' * 7]]

    for _ in range(20):
        valueArray = random.randint(1, 4)
        valueInArray = random.randint(1, 5)
        map[valueArray][valueInArray] = ' '

    for row in map:
        for columns in row:
            print(columns, end=' ')
        print()


frame()
