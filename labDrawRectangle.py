from PIL import Image, ImageDraw

im = Image.new('RGB', (500, 300), (0, 0, 0))
draw = ImageDraw.Draw(im)
draw.rectangle((200, 100, 300, 200), fill='blue')
im.save('draw-ellipse-rectangle-line.jpg', quality=95)

